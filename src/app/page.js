"use client"

import { useState } from 'react'

const HomePage = () => {

  const [file, setFile] = useState(null);
  const [imageUrl, setImageUrl] = useState("");

  const estilo = {
    margin: '2px'
  }

  const limpiarImagen = async () => await setImageUrl("")

  return (
    <center>
      <div
        style={estilo}
      >
        <form
          onSubmit={async (e) => {

            e.preventDefault(); //para evitar reincicio de pagina

            // guardar archivo en formData
            const formData = new FormData();
            formData.append('image', file)

            const response = await fetch('/api/upload', {
              method: 'POST',
              body: formData
            });

            const data = await response.json();
            setImageUrl(data.url);

          }}
        >
          <input
            type="file"
            style={estilo}
            onChange={e => {
              setFile(e.target.files[0])
            }}
          />
          <br />

          <button
            style={estilo}
            onClick={() => {
              if (imageUrl != "") limpiarImagen
            }}
          >
            Enviar
          </button>
          <br />

          <button
            style={estilo}
            onClick={() => setImageUrl("")}
          >
            Limpiar
          </button>

        </form>
        {
          imageUrl && (
            <img src={imageUrl} alt="alt" />
          )
        }
      </div>
    </center>
  )
}

export default HomePage