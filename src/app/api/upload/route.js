import {NextResponse} from 'next/server';
import { writeFile } from 'fs/promises';
import path from 'path';

import {v2 as cloudinary} from 'cloudinary';
          
cloudinary.config({ 
  cloud_name: 'dbvz6c7ki', 
  api_key: '774969549631741', 
  api_secret: 'gQNKrd84B1CF3-_dwCRK-vAbXyE' 
});

export async function POST( request ) {

  const data = await request.formData();
  const image = data.get("image");

  if (!image) {
    return NextResponse.json("no se ha subido imagen", {
      status: 400
    })
  }

  // convertir imagen en formato 'buffer'
  const bytes = await image.arrayBuffer();
  const buffer = Buffer.from(bytes);

  // const filePath = path.join(process.cwd(), 'public', image.name); //crear directorio del archivo
  // await writeFile(filePath, buffer) // guardar archivo en proyecto local

  // guardar en cloundinary
  const response = await new Promise((resolve, reject) => {
    cloudinary.uploader
      .upload_stream({}, (err, result) => {
        if (err) {
          reject(err)
        }
        resolve(result);
      }).end(buffer);
  });

  console.log(response);

  return NextResponse.json({
    message: "Imagen Subida",
    url: response.secure_url
  });
}